# {%= name %}

## Komma igång

### "Jag vill köra eller koda i projektet på min dator"

* Dubbelklicka på "Run project" i mappen `./commands/` i projektet.

### "Jag vill skicka projektet till kund"

Se [Deploy](#deploy) nedan.

## <span id="deploy">Deploy</span>

### Scorm-paket (LMS)

Det finns två byggscript att välja mellan när man ska skapa ett Scorm-paket (som bara kan köras på ett LMS):

1. Ett som använder produktionens standard-version av Scorm. Detta är det vanligaste fallet och används när en produktion bara har stöd för en Scorm-version.
2. Ett som kräver att man själv skickar in vilken Scorm-version man vill bygga för. Detta används när en produktion har stöd för flera Scorm-versioner.

#### <span id="standard-lms">1. Scormpaket med produktionens standardversion av Scorm</span>

Om produktionen bara har stöd för en Scorm-version är det detta byggscript man vill använda. Om en produktion istället har stöd för flera Scorm-versioner finns dett byggscript anpassat för detta, se [byggscriptet nedan](#specific-lms)

1. Stega versionsnumret och gör en GIT-release.
2. Kör (i roten av projektet):

```bash
grunt lms
```

En zip hamnar då i `deploy/yyyy-mm-dd/`. Denna zip är färdig att levereras till kund och är namngiven med den Scorm-version som användes vid bygget.

#### <span id="specific-lms">2. Scormpaket med dynamisk Scorm-version</span>

I produktionens `Gruntfile.js` finns en `scormVersion` angiven. Vill man bygga med denna är det enklast att använda [standardbyggscriptet ovan](#standard-lms), annars finns det möjlighet att skicka in en specifik Scormversion:

1. Stega versionsnumret och gör en GIT-release.
2. Bestäm vilken Scorm-version som ska byggas
3. Kör (i roten av projektet):

```bash
grunt lms --scormVersion=2004
```

eller

```bash
grunt lms --scormVersion=1.2
```

En zip hamnar då i `deploy/yyyy-mm-dd/`. Denna zip är färdig att levereras till kund och är namngiven med den Scorm-version som användes vid bygget.

### Webblänk (kan ej köras på ett LMS, bara på funkisweb.se)

1. Stega versionsnumret och gör en GIT-release.
2. Bestäm vilken modul som ska byggas
3. Kör (i roten av projektet):

```bash
grunt web
```

En mapp hamnar då i `deploy/yyyy-mm-dd/`. Denna mapp kan laddas upp på ftp:n och köras därifrån.
