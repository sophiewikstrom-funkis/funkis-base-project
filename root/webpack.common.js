const webpack = require("webpack");
const path = require("path");

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: require.resolve("babel-loader"),
        options: {
          // This is a feature of `babel-loader` for Webpack (not Babel itself).
          // It enables caching results in ./node_modules/.cache/babel-loader/
          // directory for faster rebuilds.
          cacheDirectory: true,
          plugins: ["react-hot-loader/babel"],
        },
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          { loader: "file-loader", options: { outputPath: "images" } },
          {
            loader: "image-webpack-loader",
            options: {
              bypassOnDebug: true,
            },
          },
        ],
      },
    ],
  },

  resolve: {
    extensions: ["*", ".js", ".jsx"],
    symlinks: false,

    // Needed when using `npm link` to use local packages. If we don't create
    // alias for single instance packages we'll end up with duplicated packages.
    alias: {
      react: path.resolve("./node_modules/react"),
      "react-dom": path.resolve("./node_modules/react-dom"),
      "funkis-framework": path.resolve("./node_modules/funkis-framework"),
    },
  },
  output: {
    path: __dirname + "dist",
    filename: "bundle.js",
  },
  devtool: "source-map",
};
