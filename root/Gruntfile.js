module.exports = function(grunt) {
  grunt.initConfig({
    config: grunt.file.readJSON("config.json"),

    projectName: "<%= config.name %>",

    projectPrefix: "<%= config.projectPrefix %>",
    projectVersion: "<%= config.version %>",

    tempFolder: "temp",
    deployFolder: "deploy/<%= grunt.template.today('yyyy-mm-dd') %>",

    // 2004 or 1.2
    scormVersion: 2004,
    basePath: "",

    env: {
      dev: {
        NODE_ENV: "development",
        NODE_PATH: "./node_modules",
      },
      dist: {
        NODE_ENV: "production",
      },
    },

    copy: {
      tempProject: {
        files: [
          {
            src: ["content/**/*", "!**/styles/**"],
            dest: "<%= tempFolder %>/",
          },
          {
            src: "src/lib/**/*",
            dest: "<%= tempFolder %>/",
          },
        ],
      },

      web_deploy: {
        files: [
          {
            expand: true,
            cwd: "<%= tempFolder %>",
            src: "**/*",
            dest: "<%= deployFolder %>/<%= projectPrefix %>-<%= buildType %>-<%= projectVersion %>/",
          },
        ],
      },
      configFile: {
        files: [
          {
            src: "./config.json",
            dest: "./config.temp.json",
          },
        ],
      },
      resetConfigFile: {
        files: [
          {
            src: "./config.temp.json",
            dest: "./config.json",
          },
        ],
      },
    },

    "json-replace": {
      config: {
        options: {
          replace: {
            storage: "<%= storage %>",
            devMode: false,
            skipIntro: false,
            showInspector: false,
            mute: false,
            autoRefresh: false,
          },
        },
        files: [
          { src: "config.json", dest: "<%= tempFolder %>/config.json" },
          { src: "config.json", dest: "./config.json" },
        ],
      },
    },

    processhtml: {
      options: {
        data: {},
      },
      distProject: {
        files: {
          "<%= tempFolder %>/index.html": ["index.html"],
        },
      },
    },
    /**
     * Generate SCORM Manifest based on scorm version.
     */
    scorm_manifest: {
      course: {
        options: {
          version: "<%= '' + scormVersion %>",
          courseId: "<%= projectPrefix %>",
          SCOtitle: "<%= projectName %>",
          launchPage: "index.html",
          path: "<%= tempFolder %>/",
          filter: "<%= tempFolder %>/",
        },
        files: [
          {
            expand: true,
            cwd: "<%= tempFolder %>/",
            src: ["**/*.*"],
            filter: "isFile",
          },
        ],
      },
    },

    /**
     * Compress SCORM package to zip
     */
    compress: {
      main: {
        options: {
          archive:
            "<%= deployFolder %>/<%= projectPrefix %>-<%= buildType %>-<%= projectVersion %>-<%= scormVersion %>.zip",
        },
        expand: true,
        cwd: "<%= tempFolder %>/",
        src: ["**/*"],
      },
    },

    /**
     * Clean temp folder where files are built before packaged
     */
    clean: {
      temp: {
        src: ["<%= tempFolder %>"],
      },
      configFile: {
        src: ["./config.temp.json"],
      },
    },

    notify: {
      dist: {
        options: {
          title: "Build complete: <%= projectName %>",
          message: "Available in ./<%= deployFolder %>/",
        },
      },
    },

    shell: {
      build: {
        command: "npm run build",
        app: "Google Chrome",
      },
      run: {
        command: "npm run start",
      },
    },
  });

  grunt.loadNpmTasks("grunt-json-replace");
  grunt.loadNpmTasks("grunt-scorm-manifest");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-env");
  grunt.loadNpmTasks("grunt-processhtml");
  grunt.loadNpmTasks("grunt-contrib-compress");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-notify");
  grunt.loadNpmTasks("grunt-shell");

  grunt.registerTask("prepareWebPackage", ["copy:web_deploy", "copy:resetConfigFile"]);

  grunt.registerTask("prepareLmsPackage", ["scorm_manifest:course", "copy:resetConfigFile", "compress:main"]);

  grunt.registerTask("cleanup", ["clean", "notify:dist"]);

  grunt.registerTask("create", function(type) {
    grunt.task.run([
      "env:dist",
      "copy:configFile",
      "setConfigValues:" + type,
      "json-replace:config",
      "shell:build",
      "copy:tempProject",
      "processhtml:distProject",
    ]);
  });

  grunt.registerTask("web", ["create:web", "prepareWebPackage", "cleanup"]);

  grunt.registerTask("lms", ["create:lms", "prepareLmsPackage", "cleanup", "testReminder"]);

  grunt.registerTask("run", ["env:dev", "shell:run"]);
  grunt.registerTask("dev", ["run"]);
  grunt.registerTask("deploy", ["create:web"]);

  function checkParameter(value, validValues, parameterName, description) {
    value = value ? value.toString() : undefined;
    var isValid = validValues.findIndex(validValue => validValue === value) > -1;

    var descriptionString = `   INVALID ${description.toString().toUpperCase()} PARAMETER FOUND                  `;

    var validValuesString = validValues.join(" or ");

    if (typeof value !== "undefined" && !isValid) {
      grunt.log.writeln("");
      grunt.log.writeln("");
      grunt.log.writeln(" ----------------------------------------------------------"["red"]);
      grunt.log.writeln(descriptionString["red"]);
      grunt.log.writeln("   Please specify its value using     "["red"]);
      grunt.log.writeln(`   (--${parameterName}=value). Valid options are ${validValuesString}.`["red"]);
      grunt.log.writeln("                                                           "["red"]);
      grunt.log.writeln(`   Alternatively, do not pass a --${parameterName} to use      `["red"]);
      grunt.log.writeln("   the default version specified in Gruntfile.js       "["red"]);
      grunt.log.writeln(" ----------------------------------------------------------"["red"]);
      grunt.log.writeln("");

      throw new Error(`Unable to create a package without a valid ${description}.`);
    }
  }

  grunt.registerTask("setConfigValues", function(type) {
    var version = grunt.option("scormVersion");
    var env = grunt.option("env");
    var platform = grunt.option("platform") === "lms" || type === "lms" ? "lms" : "web";
    var storage;
    var projectName = "<%= config.name %>";
    var basePath = "";

    checkParameter(version, ["2004", "1.2"], "scormVersion", "Scorm version");
    checkParameter(env, ["production", "development"], "env", "Environment");
    checkParameter(platform, ["lms", "web"], "platform", "Platform");

    if (!version) {
      version = grunt.config.get("scormVersion");
    }

    if (platform === "lms") {
      if (version === 2004) {
        storage = "scorm2004";
      } else {
        storage = "scorm12";
      }
    } else if (platform === "web") {
      storage = "local";
    }

    grunt.config.set("basePath", basePath);
    grunt.config.set("scormVersion", version);
    grunt.config.set("storage", storage);
    grunt.config.set("buildType", type);
    grunt.config.set("projectName", projectName);
    grunt.config.set("platform", platform);

    if (platform === "lms") {
      console.log(`Building a ${grunt.config.get("projectName")} LMS package with Scorm version ${version}`);
    } else {
      console.log(`Building a ${grunt.config.get("projectName")} web package with ${storage} storage.`);
    }
  });

  grunt.registerTask("testReminder", function() {
    grunt.log.writeln("");
    grunt.log.writeln("");
    grunt.log.writeln(" ----------------------------------------------------------");
    grunt.log.writeln("   REMEMBER TO UPLOAD AND TEST THE COURSE ON SCORM CLOUD   ");
    grunt.log.writeln("   (A kitten dies every time we ship an untested package)  ");
    grunt.log.writeln(" ----------------------------------------------------------");
    grunt.log.writeln("");
  });
};
