import React from "react";
import createReactClass from "create-react-class";

import Tabs from "./ReactSimpleTabs.jsx";

import {
  FMStartPointSelector,
  FMConfigEditor,
  FMReloadFiles,
  FMInspectorLanguageSelector,
  FMClearStores,
  FMCourseInfo,
  FMSuspendData,
} from "funkis-framework";

import StructureOverview from "./StructureOverview.jsx";

var FMInspectorContent = createReactClass({
  _onSelectedTabChanged: function(selectedIndex) {
    this.props.onAfterChange(selectedIndex);
  },

  getDefaultProps: function() {
    return {
      config: {},
      chapters: null,
      suspendData: null,
      courseStatus: "",
      currentChapterId: "",
      currentSceneId: "",
      tabActive: 0,
    };
  },

  render: function() {
    return (
      <Tabs tabActive={this.props.tabActive} onAfterChange={this._onSelectedTabChanged}>
        <Tabs.Panel title="Konfiguration">
          <FMStartPointSelector
            currentChapterId={this.props.currentChapterId}
            currentSceneId={this.props.currentSceneId}
            chapters={this.props.chapters}
            projectPrefix={this.props.config.projectPrefix || ""}
          />
          <FMReloadFiles config={this.props.config} />
          <FMConfigEditor config={this.props.config} reloadConfigFiles={false} />
          <FMInspectorLanguageSelector
            languages={this.props.config.installedLanguages}
            projectPrefix={this.props.config.projectPrefix || ""}
          />
          <FMClearStores config={this.props.config} />
        </Tabs.Panel>
        <Tabs.Panel title="Struktur">
          <StructureOverview
            currentSceneId={this.props.currentSceneId}
            currentChapterId={this.props.currentChapterId}
            chapters={this.props.chapters}
          />
        </Tabs.Panel>
        <Tabs.Panel title="Kurs">
          <FMCourseInfo courseStatus={this.props.courseStatus} />
          <FMSuspendData suspendData={this.props.suspendData} />
        </Tabs.Panel>
      </Tabs>
    );
  },
});

export default FMInspectorContent;
