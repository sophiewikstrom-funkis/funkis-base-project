import React from "react";
import createReactClass from "create-react-class";

var TestArea = createReactClass({
  mixins: [],

  getInitialState: function() {
    return {};
  },

  render: function() {
    return <div ref="container" />;
  },
});

export default TestArea;
