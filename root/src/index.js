import React from "react";
import ReactDOM from "react-dom";

import App from "./main.jsx";

import "../content/styles/styles.less";

ReactDOM.render(<App />, document.getElementById("app-container"));

if (module.hot) {
  module.hot.accept();
}
