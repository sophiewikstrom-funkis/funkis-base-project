/*
 * grunt-init
 * https://gruntjs.com/
 *
 * Copyright (c) 2016 Funkis Multimedia AB
 * Licensed under the MIT license.
 */

var http = require("http");

("use strict");

// Basic template description.
exports.description = "A Funkis template project";

exports.notes = `
Vi kommer nu sätta upp ett nytt tomt projekt. Du kommer få svara på ett par frågor och baserat det nya projektet kommer anpassas med svaren du anger.`;

// Template-specific notes to be displayed after question prompts.
exports.after = `Grattis! Projektet är nu skapat och du är redo att köra igång.

1. Radera mappen './grunt-init-scaffolding-files'. Den behövs inte längre.

2. Zippa upp 'Run project.zip' i mappen 'commands'. Radera sedan 'Run project.zip'.

3. Kör 'npm install' för att installera dependencies

4. Starta projektet med 'grun run' eller med 'Run project' från mappen 'commands'`;

// The actual init template.
exports.template = function(grunt, init, done) {
  getFunkisFrameworkTagNames().then(funkisFrameworkTagNames => {
    initialize(grunt, init, done, funkisFrameworkTagNames);
  });

  function getFunkisFrameworkTagNames() {
    return new Promise(function(resolve, reject) {
      http.get("http://192.168.0.105/api/v3/projects/165/repository/tags", function(res) {
        let rawData = "";
        res.on("data", function(chunk) {
          return (rawData += chunk);
        });

        res.on("end", function() {
          try {
            let parsedData = JSON.parse(rawData);

            var tagNames = parsedData
              .map(function(tag) {
                return { name: tag.name, date: tag.commit.committed_date };
              })
              .sort(function(a, b) {
                return new Date(b.date) - new Date(a.date);
              });

            resolve(tagNames.map(tag => tag.name));
          } catch (e) {
            reject(e.message);
          }
        });
      });
    });
  }

  function initialize(grunt, init, done, funkisFrameworkTagNames) {
    init.process(
      {},
      [
        // Prompt for these values.
        {
          name: "name",
          message: "Vad heter projektet?",
          default: "Friendly project name",
          validator: "",
          warning: "",
        },
        {
          name: "projectPrefix",
          message: "Vilket prefix (tre bokstäver, t.ex. 'exh') ska projektet ha?",
          default: "ftp",
          validator: /^[a-z]{3}$/,
          warning: "Projekt-prefixet ska besta av tre bokstäver.",
        },
        {
          name: "funkisFrameworkVersion",
          message: `Vilken version (t.ex. ${funkisFrameworkTagNames[0]}) av Funkis Framework ska användas?`,
          default: funkisFrameworkTagNames[0],
          validator: "",
          warning: "",
        },
        {
          name: "ci",
          message: "Vill du ha automatisk publicering till funkisweb? (Y/n)",
          default: "Y",
          validator: /^[yYnN]{1}$/,
          warning: "Använd Y/n för att bestämma om projektet automatiskt skall publiceras till funkisweb.",
        },
      ],
      function(err, props) {
        props.version = "0.8.0";

        // Files to copy (and process).
        var files = init.filesToCopy(props);

        if (props.ci.toLowerCase() === "n") {
          console.log("CI disabled, removing '.gitlab-ci.yml'");
          delete files[".gitlab-ci.yml"];
          delete files["exclude.txt"];
        }

        // Actually copy (and process) files.
        init.copyAndProcess(files, props, {
          noProcess: ["**/*.{png,jpg,gif,svg,mp4,mp3,zip}"],
        });

        if (funkisFrameworkTagNames.find(tagName => tagName === props.funkisFrameworkVersion)) {
          var funkisFrameworkVersion = "#" + props.funkisFrameworkVersion;
        } else {
          throw new Error(
            `Invalid version (${
              props.funkisFrameworkVersion
            }) of Funkis Framework specified. This version does not exist.`
          );
        }

        // Generate package.json file, used by npm and grunt.
        init.writePackageJSON("package.json", {
          name: props.name.replace(/\s+/g, "-").toLowerCase(),
          version: props.version,
          author: "Funkis Multimedia AB <info@funkismultimedia.se> (http://www.funkismultimedia.se/)",
          main: "index.js",
          dependencies: {
            "babel-polyfill": "^6.1.19",
            "create-react-class": "^15.6.3",
            history: "^1.13.1",
            fastclick: "^1.0.6",
            "npm-run-all": "^4.1.2",
            "prop-types": "^15.6.1",
            react: "^16.3.2",
            "react-addons-css-transition-group": "^15.1.0",
            "react-dom": "^16.3.2",
            "react-error-boundary": "^1.2.2",
            "react-router": "^3.2.0",
            "react-tween-state": "^0.1.5",
            "funkis-framework": "git+http://192.168.0.105/funkisdev/funkis-framework.git" + funkisFrameworkVersion,
          },
          devDependencies: {
            "babel-core": "^6.26.0",
            "babel-eslint": "^8.2.3",
            "babel-loader": "^7.1.4",
            "babel-preset-env": "^1.6.1",
            "babel-preset-react": "^6.24.1",
            "babel-preset-stage-2": "^6.24.1",
            "case-sensitive-paths-webpack-plugin": "^2.1.2",
            "css-loader": "^0.28.11",
            "error-overlay-webpack-plugin": "0.1.4",
            eslint: "^4.19.1",
            "eslint-config-react-app": "^2.1.0",
            "eslint-loader": "^2.0.0",
            "eslint-plugin-flowtype": "^2.46.3",
            "eslint-plugin-import": "^2.11.0",
            "eslint-plugin-jsx-a11y": "^5.1.1",
            "eslint-plugin-react": "^7.7.0",
            "file-loader": "^1.1.11",
            grunt: "^1.0.2",
            "grunt-contrib-clean": "^1.1.0",
            "grunt-contrib-compress": "^1.4.3",
            "grunt-contrib-copy": "^1.0.0",
            "grunt-env": "^0.4.4",
            "grunt-json-replace": "^0.1.2",
            "grunt-notify": "^0.4.5",
            "grunt-open": "^0.2.3",
            "grunt-processhtml": "^0.4.1",
            "grunt-scorm-manifest": "^0.2.0",
            "grunt-shell": "^2.1.0",
            "image-webpack-loader": "^4.2.0",
            less: "^3.0.2",
            "less-loader": "^4.1.0",
            "mini-css-extract-plugin": "^0.4.0",
            "react-dev-utils": "5.0.1",
            "react-hot-loader": "^4.1.1",
            "react-svg-loader": "^2.1.0",
            "style-loader": "^0.21.0",
            "uglifyjs-webpack-plugin": "^1.2.5",
            webpack: "^4.6.0",
            "webpack-cli": "^2.0.15",
            "webpack-dev-server": "^3.1.3",
            "webpack-merge": "^4.1.2",
            "webpack-notifier": "^1.6.0",
          },
          scripts: {
            test: 'echo "Error: no test specified" && exit 1',
            start:
              "webpack-dev-server --hotOnly --open --config webpack.dev.js --port 8000 --progress --colors --mode development --env.development",
            build: "webpack --config webpack.prod.js --mode production -p -o ./temp/script.js",
          },
        });

        // All done!
        done();
      }
    );
  }
};
