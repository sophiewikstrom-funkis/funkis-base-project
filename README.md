# Funkis Base Project

Detta är ett grundprojekt som används när man ska starta ett helt nytt projekt. Syftet är att ersätta det manuella arbete som utförs när ett nytt projekt startas och minimera risken för att få med sig skräp från andra produktioner.

Under installationen ställer en wizard frågor som man får besvara.

* ”Vad ska projektet heta?”,
* ”Vilket project prefix skall användas?”
* etc

Utifrån dessa svar anpassas filnamn och innehållet i filer.

## Prerequisites

* Installera nodejs

  ```sh
  $ brew install nodejs
  ```

* Installera `grunt-init`

  ```sh
  $ npm install -g grunt-init
  ```

## Hur använder jag Funkis Base Project?

1. Skapa en mapp för ditt nya projekt och navigera till den i terminalen
2. Klona ned detta projekt till en temporär undermapp. Detta görs smidigast i det terminalfönster som du precis navigerade i:

   ```
   $ git clone git@192.168.0.105:funkisdev/funkis-base-project.git ./grunt-init-scaffolding-files
   ```

3. Initiera scaffolding-filerna med `grunt-init`. Du bör då se första frågan "Vad heter projektet?" på skärmen. Skriv namnet på ditt projekt och tryck enter. Svara på alla frågor.

   ```
   $ grunt-init ./grunt-init-scaffolding-files

   Please answer the following:
   [?] Vad heter projektet? (Friendly project name) Onboarding an employee
   [?] projectPrefix oae
   [?] Do you need to make any changes to the above before continuing? (y/N) n
   ```

4. Tag bort mappen `grunt-init-scaffolding-files`. Den behövs inte längre.

   ```sh
   $ rm -rf ./grunt-init-scaffolding-files
   ```

5. Installera projektets dependencies genom att köra:

   ```sh
   $ npm install
   ```

6. Kör igång projektet genom att köra:

   > Ibland hinner inte projektets CSS-fil skapas så det kan se lite konstigt ut. Ladda bara om sidan så bör det se bättre ut.

   ```sh
   $ grunt run
   ```

## Hur uppdaterar jag Funkis Base Project?

Funkis Base Project sätts upp genom [grunt-init](http://gruntjs.com/project-scaffolding) och använder deras templatefunktioner för att modifiera våra filer så att de passar det nya projektet man vill skapa.

### Uppdatera package.json

Base-projektets `package.json` skapas upp dynamiskt i filen `template.js`. För att modifiera förinstallerade dependencies så modifierar man innehållet i funktionen `init.writePackageJSON`.

### Uppdatera statiska filer

I mappen `./root/` ligger startprojektets statiska filer. Alla filer i denna mapp går att modifiera precis som vanligt.
